# MongoDB services

## Configure

After setting up enviroment varibles at *.env* at root directory,
program reads the configure from *process.env*.

The configure reading file is [config.ts](./src/config.ts).

## Functionalities
Please refer [app.controller.ts](./src/app.controller.ts);

## How to start service

- install dependencies

```bash
npm install
```

- build file

```bash
npm run build
```

- start service

```bash
pm2 start ecosystem
```