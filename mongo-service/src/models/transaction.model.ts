interface TransactionBase {
  hash: string;
  from: string;
  to: string;
  gasUsed: number;
  gasLimit: number;
  timestamp: number;
  type: string;
  log?: any;
  gasPrice: number;
  status: boolean;
}

export interface ITx extends Partial<TransactionBase> {
  blockNumber: number;
  blockHash: string;
  transactionIndex: number;
  coinType: string;
  value: string;
  ivalue: string;
  dvalue: string;
  input: string;
  ticketNumber: number;
}
