export class BlockQueryDto {
  page: number;
  size: number;
  order?: number;
}

export interface IPagedBlock {
  txs: number;
  miner: string;
  reward: number;
  height: number;
}

export interface IBlock {
  hash: string;
  parentHash: string;
  miner: string;
  difficulty: string;
  totalDifficulty: string;
  size: string;
  gasLimit: string;
  gasUsed: string;
  timestamp: string;
  blockTime: string;
  avgGasprice: string;
  retreatTickets: string[];
  retreatMiners: string[];
  selectedTicket: string;
  ticketOrder: string;
  ticketNumber: string;
  height: number;
  reward: number;
  txs: number;
}
