import { Transport } from '@nestjs/microservices';

export const config = () => ({
  mongodb: {
    uri: process.env['mongo_uri'],
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },

  redis: {
    name: 'service:mongo',
    host: process.env.redis_host || 'localhost',
    port: process.env.redis_port || 6379,
  },

  rpc_url: process.env.rpc_url,

  app: {
    name: 'Service:MongoDB',
    transport: Transport.TCP,
    options: {
      host: process.env.app_host || '127.0.0.1',
      port: process.env.app_port || 8888,
    },
  },
});
