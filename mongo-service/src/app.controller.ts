import { Controller, Logger } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { AppService } from './app.service';
import { BlockQueryDto, IPagedBlock, IBlock, ITx } from './models';

@Controller()
export class AppController {
  private logger = new Logger(`Service:Mongo:AppController`);

  constructor(private readonly service: AppService) {}

  // get blocks by query params
  @MessagePattern('blocks')
  getBlocks(@Payload() msg: BlockQueryDto): Promise<IPagedBlock[]> {
    this.log('blocks', msg);
    return this.service.getBlocks(msg);
  }

  // get block by block height
  @MessagePattern('block')
  getBlock(@Payload() msg: { height: number }): Promise<IBlock> {
    this.log('block', msg);
    return this.service.getBlock(msg.height);
  }

  // get latest block height
  @MessagePattern('lbk')
  getLBk(): Promise<number> {
    this.log('lbk');
    return this.service.getLBkNumber();
  }

  // get a transaction by hash
  @MessagePattern('tx')
  getTransaction(@Payload() msg: { hash: string }): Promise<ITx> {
    this.log('tx', msg);
    const hash = msg.hash;
    return this.service.getTransaction(hash);
  }

  // get transaction packed by a block
  @MessagePattern('btx')
  getBlocksTransactions(@Payload() msg: { block: number }): Promise<ITx[]> {
    this.log('transactions', msg);
    return this.service.getBlocksTransactions(msg.block);
  }

  // get latest 6 blocks for home page
  @MessagePattern('l6bks')
  getL6Bks(): Promise<IBlock[]> {
    return this.service.getL6Bks();
  }

  private log(pattern: string, query?: any) {
    this.logger.log(pattern);
    if (query) {
      this.logger.log('query:');
      this.logger.verbose(JSON.stringify(query));
    }
  }
}
