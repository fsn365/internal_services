import { Module } from '@nestjs/common';
import { TransactionService } from './transaction.service';
import { MongooseModule } from '@nestjs/mongoose';
import schema from './transaction.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Transactions', schema }])],
  providers: [TransactionService],
  exports: [TransactionService],
})
export class TransactionModule {}
