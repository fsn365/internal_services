import { Document } from 'mongoose';
import { ITx } from 'src/models';

export interface TxDoc extends Partial<ITx>, Document {}
