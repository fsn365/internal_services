import { Schema } from 'mongoose';

const schema = new Schema(
  {
    _id: String,
    hash: String,
    nonce: Number,
    blockHash: String,
    blockNumber: {
      type: Number,
      index: true,
    },
    transactionIndex: Number,
    from: {
      type: String,
      index: true,
    },
    to: {
      type: String,
      index: true,
    },
    value: String,
    ivalue: String,
    dvalue: String,
    gasLimit: Number,
    gasPrice: String,
    gasUsed: Number,
    timestamp: {
      type: Number,
      index: true,
    },
    input: String,
    status: Number,
    coinType: {
      type: String,
      index: true,
    },
    type: {
      type: String,
      index: true,
    },
    log: Object,
    ticketNumber: Number,
  },
  {
    collection: 'Transactions',
    versionKey: false,
  },
);

export default schema;
