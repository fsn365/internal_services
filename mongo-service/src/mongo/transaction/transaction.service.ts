import { Injectable, BadRequestException } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { TxDoc } from './transaction.interface';
import { ITx } from '../../models';

@Injectable()
export class TransactionService {
  constructor(
    @InjectModel('Transactions') private readonly model: Model<TxDoc>,
  ) {}

  async getTransactionByHash(hash: string): Promise<ITx> {
    return this.model
      .findOne(
        { hash },
        { _id: 0, coinType: 0, ercReceipts: 0, exchangeReceipts: 0 },
      )
      .then(doc => {
        if (doc) return doc.toJSON();
        throw new BadRequestException(`Can't find  transaction hash:${hash}.`);
      });
  }

  async getBlocksTransactions(block: number): Promise<ITx[]> {
    return this.model.aggregate([
      {
        $match: { blockNumber: block },
      },
      {
        $project: { _id: 0, coinType: 0, ercReceipts: 0, exchangeReceipts: 0 },
      },
    ]);
  }
}
