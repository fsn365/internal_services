import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { BlockService } from './block.service';
import schema from './block.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: 'Blocks', schema }])],
  providers: [BlockService],
  exports: [BlockService],
})
export class BlockModule {}
