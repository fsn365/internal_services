import { Injectable, Logger, BadRequestException } from '@nestjs/common';
import BlockDoc from './block.interface';
import { InjectModel } from '@nestjs/mongoose';
import { BlockQueryDto, IPagedBlock, IBlock } from '../../models';

@Injectable()
export class BlockService {
  private logger = new Logger('Service:Mongo:Block');

  constructor(@InjectModel('Blocks') private readonly model: Model<BlockDoc>) {}

  async getBlockByHeight(height: number): Promise<IBlock> {
    this.logger.log(`Query block:${height}.`);

    return this.model
      .findOne({ number: height }, { _id: 0 })
      .then((doc: any) => {
        if (doc) return this.cleanBlock(doc.toJSON());
        throw new BadRequestException(`Can't find a block ${height}.`);
      });
  }

  async getBlocks(lBk: number, query: BlockQueryDto): Promise<IPagedBlock[]> {
    this.logger.log('Query blocks, params:\n');
    this.logger.verbose({ lBk, ...query });

    const { page, size, order = -1 } = query;
    if (page * size > lBk) {
      throw new BadRequestException(`Queried blocks are out of range.`);
    }

    let $lte: number, $gte: number;
    if (order === 1) {
      $gte = Math.max(0, (page - 1) * size);
      $lte = Math.min(lBk, $gte + size);
    }
    if (order === -1) {
      $lte = lBk - page * size;
      $gte = Math.max(0, $lte - size);
    }
    const $match = { number: { $lte, $gte } };
    const $sort = { number: +order };

    return this.model
      .aggregate([
        { $match },
        {
          $project: {
            _id: 0,
            miner: 1,
            reward: 1,
            number: 1,
            timestamp: 1,
            txcount: 1,
          },
        },
        { $sort },
        { $limit: size },
      ])
      .then(docs => docs.map(doc => this.cleanBlock(doc)));
  }

  private cleanBlock(block: any): any {
    const { reward, txcount, number, ...others } = block;
    return {
      height: number,
      ...others,
      reward: +reward / Math.pow(10, 18),
      txs: txcount,
    };
  }
}
