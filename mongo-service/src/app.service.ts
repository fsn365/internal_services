import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Interval } from '@nestjs/schedule';
import { IPagedBlock, IBlock, BlockQueryDto, ITx } from './models';
import { BlockService } from './mongo/block/block.service';
import { TransactionService } from './mongo/transaction/transaction.service';
import { RedisService } from 'nestjs-redis';

const Web3 = require('web3');
const web3FusionExtend = require('web3-fusion-extend');

@Injectable()
export class AppService {
  private web3: any;
  private redis: any;

  constructor(
    config: ConfigService,
    redisService: RedisService,
    private block: BlockService,
    private tx: TransactionService,
  ) {
    const RPC_URL = config.get('rpc_url');
    this.web3 = web3FusionExtend.extend(new Web3(RPC_URL));
    this.redis = redisService.getClient('service:mongo');
  }

  getBlock(height: number): Promise<IBlock> {
    return this.block.getBlockByHeight(height);
  }

  async getBlocks(query: BlockQueryDto): Promise<IPagedBlock[]> {
    const lBk: number = await this.getBlockHeight();
    return this.block.getBlocks(lBk, query);
  }

  getTransaction(hash: string): Promise<ITx> {
    return this.tx.getTransactionByHash(hash);
  }

  getBlocksTransactions(block: number): Promise<ITx[]> {
    return this.tx.getBlocksTransactions(block);
  }

  getL6Bks(): Promise<IBlock[]> {
    const key = this.getL6BkKey();
    return this.redis
      .get(key)
      .then((val: string) => JSON.parse(val))
      .catch(() => this.getBlocks({ page: 0, size: 6 }));
  }

  getLBkNumber(): Promise<number> {
    const key = this.getLBkKey();
    return this.redis
      .get((val: string) => +val)
      .catch(() => {
        return this.getBlockHeight();
      });
  }

  @Interval(6000)
  async getCachedValue() {
    const lBk = await this.getBlockHeight();
    const lBkKey = this.getLBkKey();
    this.redis.set(lBkKey, JSON.stringify(lBk));

    const l6Bks = await this.block.getBlocks(lBk, { page: 0, size: 6 });
    const l6BksKey = this.getL6BkKey();
    this.redis.set(l6BksKey, JSON.stringify(l6Bks));
  }

  private getBlockHeight(): Promise<number> {
    return new Promise((resolve, reject) => {
      this.web3.eth.getBlockNumber((error: any, result: number) => {
        if (error) return reject(error);
        if (result) return resolve(+result);
      });
    });
  }

  private getLBkKey(): string {
    return 'service:mongo:lbk';
  }

  private getL6BkKey(): string {
    return 'service:mongo:l6bks';
  }
}
